const express = require('express')
const db = require('../db')
const utils = require('../util')

const router = express.Router()

router.get('/',(request, response)=>{
    const query = `select id, fname, lname, password from person`
    db.pool.execute(query, (error,persons)=>{
        response.send(utils.createResult(error, persons));
    })
})

router.post('/',(request , response)=>{
    const {fname, lname, email, password} = request.body

    //encrypt password to make confedencial
   // const encryptedPassword = String(cryptoJs.MD5(password))

    const query = `insert into person (fname, lname, email, password) values (?,?,?,?)`;
    db.pool.execute(query,[fname, lname, email ,password],
        (error, result) =>
        {
            response.send(utils.createResult(error, result))
        })
})

router.delete('/',(request, response)=>{
    const {id} = request.body;
    const query = `delete from person where id=?`;
    db.pool.execute(query,[id],(error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

module.exports = router;