const express = require('express')
const cors = require('cors')

const routerPerson = require('./routes/person')

const app = express();
app.use(cors('*'))
app.use(express.json())

//add the routers
app.use('/person', routerPerson)
app.listen(4000,'0.0.0.0', ()=>{
    console.log('server started successfully on port 4000')
})

//server
//hii